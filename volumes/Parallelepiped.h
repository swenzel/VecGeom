// This file is part of VecGeom and is distributed under the
// conditions in the file LICENSE.txt in the top directory.
// For the full list of authors see CONTRIBUTORS.txt and `git log`.

/// This file includes all headers related to the Parallelepiped shape.
/// @file volumes/Parallelepiped.h

#ifndef VECGEOM_VOLUMES_PARALLELEPIPED_H_
#define VECGEOM_VOLUMES_PARALLELEPIPED_H_

#include "base/Global.h"

#include "volumes/PlacedParallelepiped.h"
#include "volumes/SpecializedParallelepiped.h"
#include "volumes/UnplacedParallelepiped.h"

#endif // VECGEOM_VOLUMES_PARALLELEPIPED_H_
