/*
 * GenTrap.h
 *
 *  Created on: Aug 3, 2014
 *      Author: swenzel
 */

#ifndef VECGEOM_VOLUMES_GENTRAP_H_
#define VECGEOM_VOLUMES_GENTRAP_H_

#include "base/Global.h"
#include "volumes/PlacedGenTrap.h"
#include "volumes/SpecializedGenTrap.h"
#include "volumes/UnplacedGenTrap.h"

#endif /* GENTRAP_H_ */
