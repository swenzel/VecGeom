// This file is part of VecGeom and is distributed under the
// conditions in the file LICENSE.txt in the top directory.
// For the full list of authors see CONTRIBUTORS.txt and `git log`.

/// This file includes all headers related to the Elliptical Tube shape
/// @file volumes/EllipticalTube.h

#ifndef VECGEOM_VOLUMES_ELLIPTICALTUBE_H_
#define VECGEOM_VOLUMES_ELLIPTICALTUBE_H_

#include "base/Global.h"

#include "volumes/PlacedEllipticalTube.h"
#include "volumes/SpecializedEllipticalTube.h"
#include "volumes/UnplacedEllipticalTube.h"

#endif // VECGEOM_VOLUMES_ELLIPTICALTUBE_H_
