// This file is part of VecGeom and is distributed under the
// conditions in the file LICENSE.txt in the top directory.
// For the full list of authors see CONTRIBUTORS.txt and `git log`.

/// This file includes all headers related to the Tet shape.
/// @file volumes/Tet.h

#ifndef VECGEOM_VOLUMES_TET_H_
#define VECGEOM_VOLUMES_TET_H_

#include "base/Global.h"

#include "volumes/PlacedTet.h"
#include "volumes/SpecializedTet.h"
#include "volumes/UnplacedTet.h"

#endif // VECGEOM_VOLUMES_TET_H_
