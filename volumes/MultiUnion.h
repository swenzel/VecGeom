/// @file MultiUnion.h
/// @author mihaela.gheata@cern.ch
//
/// Includes all headers related to the multi-union volume.

#ifndef VECGEOM_VOLUMES_MULTIUNION_H_
#define VECGEOM_VOLUMES_MULTIUNION_H_

#include "base/Global.h"
#include "volumes/PlacedMultiUnion.h"
#include "volumes/SpecializedMultiUnion.h"
#include "volumes/UnplacedMultiUnion.h"

#endif // VECGEOM_VOLUMES_MULTIUNION_H_
