// This file is part of VecGeom and is distributed under the
// conditions in the file LICENSE.txt in the top directory.
// For the full list of authors see CONTRIBUTORS.txt and `git log`.

/// This file includes all headers related to the Ellipsoid shape
/// @file volumes/Ellipsoid.h

#ifndef VECGEOM_VOLUMES_ELLIPSOID_H_
#define VECGEOM_VOLUMES_ELLIPSOID_H_

#include "base/Global.h"

#include "volumes/PlacedEllipsoid.h"
#include "volumes/SpecializedEllipsoid.h"
#include "volumes/UnplacedEllipsoid.h"

#endif // VECGEOM_VOLUMES_ELLIPSOID_H_
