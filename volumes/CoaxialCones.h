/// @file CoaxialCones.h
/// @author Raman Sehgal (raman.sehgal@cern.ch)

#ifndef VECGEOM_VOLUMES_COAXIALCONES_H_
#define VECGEOM_VOLUMES_COAXIALCONES_H_

#include "base/Global.h"

#include "volumes/PlacedCoaxialCones.h"
#include "volumes/SpecializedCoaxialCones.h"
#include "volumes/UnplacedCoaxialCones.h"

#endif // VECGEOM_VOLUMES_COAXIALCONES_H_
