/// Includes all headers related to the simple extruded volume.

#ifndef VECGEOM_VOLUMES_SEXTRU_H_
#define VECGEOM_VOLUMES_SEXTRU_H_

#include "base/Global.h"
#include "volumes/PlacedSExtru.h"
#include "volumes/SpecializedSExtru.h"
#include "volumes/UnplacedSExtruVolume.h"

#endif // VECGEOM_VOLUMES_SEXTRU_H_
