// This file is part of VecGeom and is distributed under the
// conditions in the file LICENSE.txt in the top directory.
// For the full list of authors see CONTRIBUTORS.txt and `git log`.

/// This file includes all headers related to the Paraboloid shape.
/// @file volumes/Paraboloid.h

#ifndef VECGEOM_VOLUMES_PARABOLOID_H_
#define VECGEOM_VOLUMES_PARABOLOID_H_

#include "base/Global.h"

#include "volumes/PlacedParaboloid.h"
#include "volumes/SpecializedParaboloid.h"
#include "volumes/UnplacedParaboloid.h"

#endif // VECGEOM_VOLUMES_PARABOLOID_H_
