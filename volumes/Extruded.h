/// @file Extruded.h
/// @author mihaela.gheata@cern.ch
//
/// Includes all headers related to the extruded volume.

#ifndef VECGEOM_VOLUMES_EXTRUDED_H_
#define VECGEOM_VOLUMES_EXTRUDED_H_

#include "base/Global.h"
#include "volumes/PlacedExtruded.h"
#include "volumes/SpecializedExtruded.h"
#include "volumes/UnplacedExtruded.h"

#endif // VECGEOM_VOLUMES_EXTRUDED_H_
