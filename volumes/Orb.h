// This file is part of VecGeom and is distributed under the
// conditions in the file LICENSE.txt in the top directory.
// For the full list of authors see CONTRIBUTORS.txt and `git log`.

/// \brief This file includes all headers related to the Orb shape
/// \file volumes/Orb.h
/// \author Raman Sehgal

#ifndef VECGEOM_VOLUMES_ORB_H_
#define VECGEOM_VOLUMES_ORB_H_

#include "base/Global.h"

#include "volumes/PlacedOrb.h"
#include "volumes/SpecializedOrb.h"
#include "volumes/UnplacedOrb.h"

#endif // VECGEOM_VOLUMES_ORB_H_
